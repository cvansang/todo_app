class Task < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validates_presence_of :title
  validate :future_completed_date

  private

    def future_completed_date
      if !completed.blank? && completed > Date.today
        self.errors.add(:completed, "can't be in the future")
      end
    end
end
