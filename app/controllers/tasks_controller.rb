class TasksController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :destroy, :edit, :update]
  before_action :correct_user,   only: [:destroy, :edit, :update]

  def new
    @task = current_user.tasks.build
    render :show_form
  end
  
  def create
    @task = current_user.tasks.build(task_params)
    if @task.save
      flash[:success] = "New task added!"
      redirect_to root_url
    else
      @feed_items = []
      redirect_to root_url
    end
  end

  def destroy
    @task.destroy
    flash[:success] = "Task deleted"
    redirect_to request.referrer || root_url
  end

  def edit
    @task = Task.find(params[:id])
    render :show_form
  end

  def update
    @task = Task.find(params[:id])
    @task.update_attributes(task_params)
    if @task.save
      flash[:success] = "Task updated!"
      redirect_to root_url
    else
      redirect_to root_url
    end
  end

  private
  
    def task_params
      params.require(:task).permit(:title, :content, :completed)
    end

    def correct_user
      @task = current_user.tasks.find_by(id: params[:id])
      redirect_to root_url if @task.nil?
    end
end
